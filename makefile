# find all .md files in the directory
# mdsrc=$(shell ls *.md)
mdsrc=$(shell find . -iname "*.md")
html_from_md=$(mdsrc:%.md=%.html)
pdf_from_md=$(mdsrc:%.md=%.pdf)


all: $(html_from_md) $(pdf_from_md)

html_from_md: $(html_from_md)

fixnames:
	rename "s/ /_/g" *

today:
	touch `date +"%Y-%m-%d.md"`

now_folder:
	mkdir `date +"%Y-%m-%d-%H%M%S"`

# Implicit rule to know how to make .html from .md
%.html: %.md
	pandoc --from markdown \
		--to html \
		--standalone \
		--css styles.css \
		-i $< $@

%.320x.jpg: %.png
	convert -resize 320x $< $@

# special rule for debugging variables
print-%:
	@echo '$*=$($*)'

dist/leafygal.js: src/leafygal.js
	node_modules/.bin/webpack

dist/dynatiler.js: src/dynatiler.js
	node_modules/.bin/webpack --config dynatiler.config.js

%.html: %.md
	pandoc --from markdown \
		--to html \
		--standalone \
		--listings \
		--css styles.css \
		$< -o $@

%.pdf: %.md
	scripts/include.py $< | \
	pandoc --from markdown \
		--standalone \
		--listings \
		-o $@
