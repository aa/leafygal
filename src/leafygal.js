var async = require("async");

/*

The main idea is the implementation of a "tiler" function
that satisfies serves as a "createTile" method of a leaflet GridLayer.
See <http://leafletjs.com/reference-1.3.0.html#gridlayer>

the tiler function has signature (coords, done) =>

    where coords is of form {x:0, y:0, z:0 }
    and done should be called when the element is ready to be displayed
    (e.g. any contained images are loaded)

*/

function simpletemplate (str, vars) {
    // simpletemplate("**{foo}{bar}**", {foo: "hello", bar: 17}) => "**hello17**" 
    return str.replace(/{(\w+?)}/g, function (m, p1, offset, str) {
        return vars[p1];
    })
}

/*

tiler is the base tiler for a single image represented by an object of the form: 

    { tiles: "path/to/{x}{y}{z}.jpg", id: "/link/href", name: "link label" }

*/
function tiler (item) {
    return function (coords, done) {
        var imgsrc = simpletemplate(item.tiles, coords),
            img = document.createElement("img"),
            ret = img,
            a;
        if (coords.x == 0 && coords.y == 0 && coords.z >= 0) {
            ret = document.createElement("div");
            ret.appendChild(img);
            a = document.createElement("a");
            a.href = item.id;
            a.innerHTML = item.name;
            ret.appendChild(a);
        }
        img.src = imgsrc;
        img.addEventListener("load", function () {
            done();
        });
        return ret;
    }
}

// dynamic tiler?!
// given an image at high res, dynamically tile it
function dynatiler (url, cell_width, cell_height) {
    if (cell_width == undefined) { cell_width = 256; }
    if (cell_height == undefined) { cell_height = 256; }
    return function (coords, done) {
        var img = document.createElement("img"),
            retimg = document.createElement("img"),
            canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d");
        canvas.width = cell_width;
        canvas.height = cell_height;
        img.crossOrigin = "Anonymous";      
        img.src = url;
        img.addEventListener("load", function () {
            // draw the image to the canvas
            // using coordinates (todo)
            ctx.drawImage(img, 0, 0, cell_width, cell_height);
            retimg.src = canvas.toDataURL();
            retimg.addEventListener("load", function () {
                done();

            })
        });
        if (coords.x == 0 && coords.y == 0 && coords.z == 0) {

        }
        return retimg;
    }
}

function gridlayer (L, tiler, opts) {
    var klass = L.GridLayer.extend({ createTile: tiler });
    return new klass(opts);
}

function emptyelement (done) {
    window.setTimeout(function () { done() }, 0);
    return document.createElement("div");
}

function someelement (elt, done) {
    window.setTimeout(function () { done() }, 0);
    return elt;
}

/*
A "nop" tiler... Just an empty div
*/
function emptytiler (coords, done) {
    window.setTimeout(function () { done() }, 0);
    return document.createElement("div");
}

function fourup (t1, t2, t3, t4, cell_width, cell_height) {
    var allitems = [],
        cached_image = null;
    if (t1) {allitems.push(t1);}
    if (t2) {allitems.push(t2);}
    if (t3) {allitems.push(t3);}
    if (t4) {allitems.push(t4);}

    return function (coords, done) {
        var x = coords.x,
            y = coords.y,
            z = coords.z,
            hcols;
        if (z > 0) {
            hcols = Math.pow(2, z-1);
            if (x >= 0 && x < hcols*2 && y>=0 && y<hcols*2) {
                if (x < hcols) {
                    if (y < hcols) {
                        return t1 ? t1({x:x, y:y, z:z-1}, done) : emptyelement(done);
                    } else {
                        return t3 ? t3({x:x, y:y-hcols, z:z-1}, done) : emptyelement(done);
                    }
                } else {
                    if (y < hcols) {
                        return t2 ? t2({x:x-hcols, y:y, z:z-1}, done) : emptyelement(done);
                    } else {
                        return t4 ? t4({x:x-hcols, y:y-hcols, z:z-1}, done) : emptyelement(done);
                    }
                }

            } else {
                return emptyelement(done);
            }
        } else if (z == 0 && x == 0 && y == 0) {
            // console.log("fourup", cached_image);
            if (cached_image !== null) {
                // console.log("using cached image", cached_image);
                var ret = document.createElement("img");
                ret.addEventListener("load", function () {
                    // console.log("[fourup] cached image LOAD");
                    done();
                });
                ret.src = cached_image;
                return ret;
                // window.setTimeout(function () {
                //     done()
                // }, 0);
                // return cached_image;
            }
            // Generate a thumbnail of the child elements
            // Use async to ensure each element is ready
            // then use a canvas to scale & draw the combined elements
            var ret = document.createElement("img");
            async.map(allitems, function (x, next) {    // done == next => (err, result)
                var ret = x({x: 0, y: 0, z: 0}, function () {
                    next(null, ret);
                });
            }, function (err, images) {
                if (err) {
                    console.log("fourup: ERROR loading child images", err);
                    // return emptyelement(done);
                }
                // console.log("[fourup] All images ready", images);
                var canvas = document.createElement("canvas"),
                    ctx = canvas.getContext("2d"),
                    r = 0,
                    c = 0;
                canvas.width = cell_width;
                canvas.height = cell_height;
                var half_cell_width = cell_width/2,
                    half_cell_height = cell_height/2;
                images.forEach(function (elt) {
                    var img = elt.nodeName == "IMG" ? elt : elt.querySelector("img");
                    if (img) {
                        ctx.drawImage(img, c*half_cell_width, r*half_cell_height, half_cell_width, half_cell_height);
                    }
                    c += 1;
                    if (c == 2) { c = 0; r += 1; }
                });
                ret.addEventListener("load", function () {
                    // cached_image = ret.src;
                    // discover ... ret.src seems to be invalid at this point -- weirdly the image seems to get "released" once
                    // drawn...
                    done();    
                });
                ret.src = canvas.toDataURL();
                // jan 2018: new style image caching: cache the data url string directly, NOT the DOM img element
                cached_image = ret.src;
                // console.log("[fourup] caching", cached_image);
            });
            return ret;
        } else {
            return emptyelement(done);            
        }     
    }
}

var split4 = function(items) {
    var c, el, i, l, len, p, ref, results, x;
    l = items.length;
    p = Math.ceil(Math.log(l) / Math.log(4));
    c = Math.max(1, Math.pow(4, p) / 4);
    el = function(x, c) {
      while (x.length < c) {
        x.push(null);
      }
      return x;
    };
    ref = [items.slice(0, c), items.slice(c, c * 2), items.slice(c * 2, c * 3), items.slice(c * 3)];
    results = [];
    for (i = 0, len = ref.length; i < len; i++) {
      x = ref[i];
      results.push(el(x, c));
    }
    return results;
};

function gridlayout (items, cell_width, cell_height) {
    if (cell_width == undefined) { cell_width = 256; }
    if (cell_height == undefined) { cell_height = 256; }
    if (items.length == 1) {
        return items[0].tiles ? tiler(items[0]) : dynatiler(items[0]);
    } else {
        var l = items.length;
        var p = Math.ceil(Math.log(l) / Math.log(4));
        var c = Math.max(1, Math.pow(4, p) / 4);
        var split = [items.slice(0, c), items.slice(c, c * 2), items.slice(c * 2, c * 3), items.slice(c * 3)];
        var t = split.map(function (x) {
            if (x.length > 0) { return gridlayout(x); }
        });
        return fourup (t[0], t[1], t[2], t[3], cell_width, cell_height); 
    }
}

// take a flat list of positioned items in the form {x: 0, y: 2, item: tilableobj} (ie all at z=0)
// tilable == that which can be passed to the (base) tiler function
// based on width/height, calculate final depth and split / recalculate the next layer
// return fourup of recursive layout of shifted items representing a quarter of the layout
// requires: all element positions are unique (no duplicates!)
// right & bottom represent the last used coordinate (thus numcols = right - left + 1)
// var LIMIT = 100;
function layout (positioneditems, left, top, right, bottom, level, cell_width, cell_height) {
    if (cell_width === undefined) { cell_width = 256; }
    if (cell_height === undefined) { cell_height = 256; }
    // turn arbitrary layout into fourup of other layout functions
    // if (--LIMIT < 0) return;
    if (level === undefined) { level = 0; }
    var indent = '';
    for (var i=0; i<level; i++) { indent += '  '; }
    var i,
        l,
        pi,
        cols,
        rows;

    if (left === undefined) {
        for (i=0, l=positioneditems.length; i<l; i++) {
            pi = positioneditems[i];
            if (left === undefined || pi.x < left) { left = pi.x; }
            if (right === undefined || pi.x > right) { right = pi.x; }
            if (top === undefined || pi.y < top) { top = pi.y; }
            if (bottom === undefined || pi.y > bottom) { bottom = pi.y; }
        }
        // remember to use left + top to shift elements
    }
    cols = (right - left) + 1;
    rows = (bottom - top) + 1;
    // bump up to next power of 2
    var size = Math.max(cols, rows),
        n = Math.ceil(Math.log2(size)),
        ssize = Math.pow(2, n),
        hsize = ssize/2;

    // console.log(indent+"layout", positioneditems.length, "items", "left", left, "top", top, "right", right, "bottom", bottom, "hsize", hsize, "ssize", ssize);
    // console.log("ssize", ssize, "hsize", hsize);
    // Base case
    if (positioneditems.length == 0) {
        return emptytiler;
    }
    if (size == 1 && positioneditems.length == 1) {
        // console.log("base case");
        return positioneditems[0].item.tiles ? tiler(positioneditems[0].item) : dynatiler(positioneditems[0].item);
    }

    // Create 4 new positioned element cells, each hsize x hsize
    // and wrap in a recursive call to layout
    var lefttop = [],
        righttop = [],
        leftbottom = [],
        rightbottom = [];
    for (var i=0,l=positioneditems.length; i<l; i++) {
        pi = positioneditems[i];
        if (pi.x - left < hsize) { // left
            if (pi.y - top < hsize) { // top
                lefttop.push(pi)
            } else { // bottom
                leftbottom.push(pi)
            }
        } else { // right
            if (pi.y - top < hsize) { // top
                righttop.push(pi);
            } else { // bottom
                rightbottom.push(pi);
            }
        }
    }
    // console.log(indent+"recursing...", lefttop.length, righttop.length, leftbottom.length, rightbottom.length);
    return fourup(
        layout(lefttop,     left,       top,       left+hsize-1, top+hsize-1, level+1, cell_width, cell_height),
        layout(righttop,    left+hsize, top,       left+ssize-1, top+hsize-1, level+1, cell_width, cell_height),
        layout(leftbottom,  left,       top+hsize, left+hsize-1, top+ssize-1, level+1, cell_width, cell_height),
        layout(rightbottom, left+hsize, top+hsize, left+ssize-1, top+ssize-1, level+1, cell_width, cell_height),
        cell_width,
        cell_height
    );
}


module.exports = {
    gridlayout: gridlayout,
    gridlayer: gridlayer,
    layout: layout
}

