Usage
==========

Use python to ensure image tiles and output a JSON index.

    python imagetile.py *.JPG > gallery.json


The gallery layer is then initialized with the data...

```javascript
var data = JSON.parse(request.responseText);
console.log("data", data);
var vt = leafygal.gridlayout(data['@graph']);
var layer = leafygal.gridlayer(L, vt);
map.addLayer(layer);
```

<a title="By James Rosindell (Own work) [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3ALeafy_Seadragon_on_Kangaroo_Island.jpg"><img width="512" alt="Leafy Seadragon on Kangaroo Island" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Leafy_Seadragon_on_Kangaroo_Island.jpg/512px-Leafy_Seadragon_on_Kangaroo_Island.jpg"/></a>

Live examples
==================
* <http://observatory.constantvzw.org/images/gallery.html>
* <http://images.constantvzw.org/Techno-Galactic-Software-Observatory/gallery.html>


Documentation
=================

gridlayout takes a list of items.

an item may be a tiled image:

    {"id": "photo/04/foo.jpg", "name": "foo", "tiles": "photo/04/tiles/foo/z{z}y{y}x{x}.jpg" }

or a named list of items:

    {"id": "photo/04/", "name": "04", "items": [...] }


Challenge to create a JSON-LD dump of a single folder that can then be combined via json-ld merge and form a (flattened) hierarchy that can then be rendered with the (modified) gridlayout.

Missing piece is to give an identity to the "containing" element which then allows linking (from above).

Imagine:

    ld-ls:
    {
        '@context': {
            baseURI: "http://automatist.org/photo/"
        }
        'id': "",
        'items': [
            {
                'id': "photo/2006",
                'type': 'Collection'
            },
            ...
            {
                'id': "DVD-Regions_with_key.png",
                'type': 'Image',
                'tiles': "tiles/DVD-Regions_with_key.png/z{z}y{y}x{x}.jpg"
            }
        ]
    }

and:

    {
        '@context': {
            baseURI: "http://automatist.org/photo/2006/"
        }
        'id': "",
        'items': [
            {
                'id': "2006-10-14",
                'type': 'Collection'
            },
            ...
        ]
    }

