const path = require('path');
// const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
    entry: "./src/dynatiler.js",
    mode: "development",
    output: {
        filename: "dynatiler.js",
        path: path.resolve(__dirname, 'dist'),
        library: "dynatiler"
    }
    // plugins: [
    //     new UglifyJSPlugin()
    // ]
}