const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
    entry: "./src/leafygal.js",
    output: {
        filename: "leafygal.js",
        path: path.resolve(__dirname, 'dist'),
        library: "leafygal"
    }
    // plugins: [
    //     new UglifyJSPlugin()
    // ]
}